const modalSearch = () => {
  const searchButton = document.querySelector(".btn-search");
  const modalSearch = document.querySelector(".modal-search");
  const modalBackground = document.querySelector(".modal-search__background");

  searchButton.addEventListener("click", () => {
    modalSearch.classList.toggle("active");
    modalBackground.classList.toggle("active");
  });

  modalBackground.addEventListener("click", () => {
    modalSearch.classList.remove("active");
    modalBackground.classList.remove("active");
  });
};

const modalRegion = () => {
  const regionButton = document.querySelector(".btn-city");
  const yesRegion = document.getElementById("yes_region");
  const modalRegion = document.querySelector(".modal-region");

  regionButton.addEventListener("click", () => {
    modalRegion.classList.toggle("active");
  });

  yesRegion.addEventListener("click", () => {
    modalRegion.classList.remove("active");
  });
};

export { modalSearch, modalRegion };
