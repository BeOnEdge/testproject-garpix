const sliderCheck = () => {
  const toggleCheckbox = document.getElementById("toggle-checkbox");
  const leftText = document.getElementById("left-text");
  const rightText = document.getElementById("right-text");

  if (toggleCheckbox && leftText && rightText) {
    function updateTextColor() {
      leftText.classList.toggle("text-green", toggleCheckbox.checked);
      rightText.classList.toggle("text-green", !toggleCheckbox.checked);
    }
    updateTextColor();

    toggleCheckbox.addEventListener("change", updateTextColor);

    leftText.addEventListener("click", () => {
      if (toggleCheckbox.checked) {
        toggleCheckbox.checked = !toggleCheckbox.checked;
        toggleCheckbox.dispatchEvent(new Event("change"));
        updateTextColor();
      }
    });

    rightText.addEventListener("click", () => {
      if (!toggleCheckbox.checked) {
        toggleCheckbox.checked = !toggleCheckbox.checked;
        toggleCheckbox.dispatchEvent(new Event("change"));
        updateTextColor();
      }
    });
  }
};

const morePartners = () => {
  const buttonMorePartn = document.getElementById("button-more-partn");

  if (buttonMorePartn) {
    buttonMorePartn.addEventListener("click", function() {
      const hiddenCards = document.querySelectorAll(".card.hidden");
      const visibleCards = document.querySelectorAll(".card.show");

      hiddenCards.forEach((card) => {
        card.classList.remove("hidden");
        card.classList.add("show");
        buttonMorePartn.textContent = "Скрыть";
      });
      visibleCards.forEach((card) => {
        card.classList.remove("show");
        card.classList.add("hidden");
        buttonMorePartn.textContent = "Еще 4 партнера";
      });
    });
  }
};

const checboxAcceptThanks = () => {
  const checkbox = document.getElementById("toggle-checkbox");
  const creditThanks = document.querySelector(".credit-thanks");
  const acceptThanks = document.querySelector(".accept-thanks");

  if (checkbox && creditThanks && acceptThanks) {
    checkbox.addEventListener("change", () => {
      creditThanks.classList.toggle("active");
      acceptThanks.classList.toggle("active");
    });
  }
};

export { sliderCheck, morePartners, checboxAcceptThanks };
